const EMAIL_EXISTS = "Email has already exists.";
const INVALID_CREDENTIAL = "Email or password is incorrect";

module.exports = {
  EMAIL_EXISTS,
  INVALID_CREDENTIAL,
};